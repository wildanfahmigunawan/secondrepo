from django import forms
from django.utils.translation import ugettext_lazy

class Message_Form(forms.Form):
    my_error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    website = forms.CharField(label='Website',widget=forms.TextInput(attrs=attrs), required=False)
    email = forms.EmailField(required=False, widget=forms.EmailInput(attrs=attrs))
    message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)
