from django.shortcuts import render
from lab_1.views import mhs_name, birth_date
#Create a list of biodata that you wanna show on webpage:
#[{'subject' : 'Name', 'value' : 'Igor'},{{'subject' : 'Birth Date', 'value' : '11 August 1970'},{{'subject' : 'Sex', 'value' : 'Male'}
#TODO Implement
bio_dict = [{'subject' : 'Tanggal Lahir', 'value' : '1 August 1998'},{'subject' : 'Sex', 'value' : 'Male'}
,{'subject' : 'Jurusan', 'value' : 'Ilmu Komputer'},{'subject' : 'Semester', 'value' : 'Tiga'}
,{'subject' : 'Telepon', 'value' : '087741463534'},{'subject' : 'Email', 'value' : 'wildanfahmigunawan@gmail.com'}
,{'subject' : 'Hobby', 'value' : 'Travelling'},{'subject' : 'Moto', 'value' : 'Be The Best You Can Be'}]

def index(request):
    response = {"bio_dict": bio_dict}
    return render(request, 'description_lab2addon.html', response)
