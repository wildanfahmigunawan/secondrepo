var counter =0;
$(document).ready(function(){
    // kode jQuery selanjutnya akan ditulis disini
    $(document).keypress(function(e) {
        if(e.which == 13) {
            var message = $('textarea').val();
            var old = $('.msg-insert').html();
            if(counter%2===0){
            $('.msg-insert').html(old + '<p class="msg-send">' +message + '</p>');}
            else {
                $('.msg-insert').html(old + '<p class="msg-receive">' +message + '</p>'); 
            }
            $('textarea').val(null);
            $('textarea').empty();
        }
    });
});

var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
      print.value = "";
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'sin') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'tan') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'log') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}


function changeColor(x){
    $('body').css({'backgroundColor':x['bcgColor']});
    $('.text-center').css({'color':x['fontColor']});
};

if (localStorage.getItem('themes') === null){localStorage.setItem('themes','[{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}]');}
var themes = JSON.parse(localStorage.getItem('themes'));

if (localStorage.getItem('selectedTheme') === null){localStorage.setItem('selectedTheme',JSON.stringify(themes[3]));}
var theme = JSON.parse(localStorage.getItem('selectedTheme'));

changeColor(theme);

$(document).ready(function() {
    $('.my-select').select2({'data' : themes}).val(theme['id']).change();
    $('.apply-button').on('click', function(){  // sesuaikan class button
        // [TODO] ambil value dari elemen select .my-select
        theme = themes[$('.my-select').val()];
        changeColor(theme);
        // [TODO] simpan object theme tadi ke local storage selectedTheme
        localStorage.setItem("selectedTheme",JSON.stringify(theme));
    })
    

});
// END

