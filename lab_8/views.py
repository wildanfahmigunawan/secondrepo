from django.shortcuts import render

response = {}
def index(request):
    response['author'] = 'Wildan Fahmi Gunawan'
    return render(request, 'lab_8/lab_8.html', response)

# Create your views here.
